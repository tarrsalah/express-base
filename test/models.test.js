const { sequelize, User } = require('../src/models');

describe('Models', done => {
  beforeEach(async () => {
    await sequelize.sync({ force: true });
  });

  after(async () => {
    await sequelize.sync({ force: true });
  });

  it('Fetch all users from teh database', async () => {
    const users = await User.findAll();
  });

  it('Create a user and save it to the database', async () => {
    const user = await User.create({
      firstName: 'Taouririt',
      lastName: 'Salah Eddine',
      email: 'tarrsalah@gmail.com',
      password: 'mypassword'
    });
  });
});
