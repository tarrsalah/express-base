exports.flash = () => (req, res, next) => {
  res.locals.flashSuccess = req.flash('success');
  res.locals.flashError = req.flash('error');
  res.locals.flashWarning = req.flash('warning');
  res.locals.flashInfo = req.flash('info');
  next();
};
