const router = require('express').Router();
const passport = require('../passport.js');
const bcrypt = require('bcrypt');
const { check, validationResult } = require('express-validator/check');
const { User } = require('../models');

router.get('/login', (req, res) => {
  res.render('account/login.ejs');
});

router.post(
  '/login',
  passport.authenticate('local', {
    successRedirect: '/',
    successFlash: true,
    failureRedirect: '/account/login',
    failureFlash: true
  })
);

router.post('/logout', (req, res) => {
  req.logout();
  req.flash('info', 'You logged out!');
  res.redirect('/');
});

router.get('/register', (req, res) => {
  res.render('account/register.ejs');
});

router.post(
  '/register',
  [
    check('firstName').isLength({ min: 3 }),
    check('lastName').isLength({ min: 3 }),
    check('email').isEmail(),
    check('password')
      .isLength({ min: 8 })
      .withMessage('The password must be at least 8 characters'),
    check('confirmedPassword')
      .exists()
      .custom((value, { req }) => value === req.body.password)
      .withMessage('The confirmed password does not match')
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.render('account/register.ejs', {
        errors: errors.mapped(),
        values: req.body
      });
    }

    try {
      const result = await User.findOrCreate({
        where: { email: req.body.email },
        defaults: {
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          password: req.body.password
        }
      });

      const user = result[0];
      const isCreated = result[1];

      if (!isCreated) {
        res.render('account/register.ejs', {
          errors: { email: { msg: 'Email already exists' } },
          values: req.body
        });
      } else {
        req.login(user, err => {
          if (!err) {
            res.redirect('/account/manage/info');
          } else {
            res.status(501).send(err);
          }
        });
      }
    } catch (err) {
      res.status(501).send(err);
    }
  }
);

router.get('/manage/info', (req, res) => {
  const { user } = req;
  res.render('account/manage/info.ejs', { user });
});

router.get('/manage/change-email', (req, res) => {
  res.render('account/manage/change-email.ejs');
});

router.post(
  '/manage/change-email',
  [
    check('email').isEmail(),
    check('password')
      .custom((password, { req }) => {
        const { user } = req;
        return bcrypt.compareSync(password, user.password);
      })
      .withMessage('Password is not correct')
  ],

  (req, res) => {
    const errors = validationResult(req);
    const email = req.body.email;
    const user = req.user;

    if (!errors.isEmpty()) {
      return res.render('account/manage/change-email.ejs', {
        values: { email },
        errors: errors.mapped()
      });
    }

    User.update({ email }, { where: { email: user.email } })
      .then(result => {
        user.email = email;
        req.logIn(user, err => {
          if (err) {
            throw err;
          }
          req.flash('success', 'Email has been changed!');
          return res.redirect('/account/manage/info');
        });
      })
      .catch(err => {
        res.sendStatus(500);
      });
  }
);

router.get('/manage/change-password', (req, res) => {
  res.render('account/manage/change-password.ejs');
});

module.exports = router;
