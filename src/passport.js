const bcrypt = require('bcrypt');
const passport = require('passport');
const Stategy = require('passport-local').Strategy;
const { User } = require('./models');

passport.use(
  new Stategy(
    {
      usernameField: 'email',
      passwordField: 'password'
    },
    function(email, password, done) {
      User.findOne({
        where: {
          email: email
        }
      }).then(function(user, err) {
        if (err) {
          return done(null, false, { message: 'Invalid email or password' });
        }

        if (!user || !bcrypt.compareSync(password, user.password)) {
          return done(null, false, { message: 'Invalid email or password' });
        }

        return done(null, user, {
          message: 'You are now logged in. Welcome back!'
        });
      });
    }
  )
);

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

module.exports = passport;
