const express = require('express');
const path = require('path');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const RedisStore = require('connect-redis')(session);
const passport = require('./passport.js');
const connectFlash = require('connect-flash');
const flash = require('./middlewares').flash;
const accounts = require('./routes/account.js');

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '../views'));
app.use(express.static(path.join(__dirname, '../public')));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  session({
    store: new RedisStore({
      host: 'redis'
    }),
    key: 'user_id',
    secret: 'very_secret',
    resave: false,
    saveUninitialized: false,
    unset: 'destroy'
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(connectFlash());
app.use(flash());

app.use((req, res, next) => {
  res.locals.activeFor = path => {
    return path === req.originalUrl ? 'active' : '';
  };

  const isAuthenticated = req.isAuthenticated();
  res.locals.isAuthenticated = isAuthenticated;
  if (isAuthenticated) {
    res.locals.user = {
      username: req.user.username
    };
  }
  next();
});

app.use('/account', accounts);

app.get('/', (req, res) => {
  return res.render('index.ejs');
});

module.exports = app;
