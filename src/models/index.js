const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');
const conf = require('../../database/conf.js');

conf.logging = false;
conf.operatorsAliases = Sequelize.Op;

const sequelize = new Sequelize(conf);

const hashPasswordHook = user => {
  const salt = bcrypt.genSaltSync(10);
  user.password = bcrypt.hashSync(user.password, salt);
};

const User = sequelize.define(
  'user',
  {
    firstName: { type: Sequelize.STRING(50) },
    lastName: Sequelize.STRING(50),
    email: Sequelize.STRING(255),
    password: Sequelize.CHAR(60)
  },
  {
    hooks: {
      beforeCreate: hashPasswordHook,
      beforeUpdate: hashPasswordHook
    }
  }
);

module.exports = {
  sequelize,
  User
};
