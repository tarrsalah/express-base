const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = {
  context: path.resolve('assets/scripts'),
  entry: {
    app: './index.js'
  },
  output: {
    path: path.resolve('public'),
    filename: 'bundle.js'
  },
  devtool: 'source-map',

  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.(scss)$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader' // translates CSS into CommonJS modules
            },
            {
              loader: 'postcss-loader', // Run post css actions
              options: {
                plugins: function() {
                  return [require('precss'), require('autoprefixer')];
                },
                sourceMap: true
              }
            },
            {
              loader: 'sass-loader', // compiles Sass to CSS
              options: { sourceMap: true }
            }
          ]
        })
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({ filename: 'bundle.css' }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    })
    // new webpack.optimize.UglifyJsPlugin({
    //   sourceMap: true
    // })
  ],
  performance: {
    hints: false
  }
};

module.exports = config;
